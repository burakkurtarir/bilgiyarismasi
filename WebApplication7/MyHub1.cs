﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using WebApplication7.Models;

namespace WebApplication7
{
    public class MyHub1 : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }

        public void MesajGonder(string mesaj)
        {
            Clients.All.mesajGonder(mesaj);
        }

        public void SoruyuGonder(Object soru)
        {
            YarismaTakibi.sonKaldigimizSoruNumarasi++;
            Clients.All.soruyuGonder(soru);
        }

        public void DogruYanlisSayisiniGoster()
        {
            int dogruSayisi = YarismaTakibi.dogruYapanlarinSayisi[YarismaTakibi.sonKaldigimizSoruNumarasi];
            int yanlisSayisi = YarismaTakibi.yanlisYapanlarinSayisi[YarismaTakibi.sonKaldigimizSoruNumarasi];
            Clients.All.dogruYanlisSayisiniGoster(dogruSayisi, yanlisSayisi);
        }

        public void KazandiMi()
        {
            Clients.All.kazandiMi();
        }
    }
}