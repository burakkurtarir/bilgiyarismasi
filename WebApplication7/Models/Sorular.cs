﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson.Serialization.Attributes;

namespace WebApplication7.Models
{
    public class Sorular
    {
        [BsonElement("soruNumarasi")]
        public int soruNumarasi { get; set; }

        [BsonElement("soruAdi")]
        public string soruAdi { get; set; }

        [BsonElement("cevap")]
        public string cevap { get; set; }

        [BsonElement("siklar")]
        public string[] siklar { get; set; }

        [BsonElement("dogruYapanlarinSayisi")]
        public int dogruYapanlarinSayisi { get; set; }

        [BsonElement("yanlisYapanlarinSayisi")]
        public int yanlisYapanlarinSayisi { get; set; }

        public Sorular()
        {
            siklar = new string[3];
        }
    }
}