﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication7.Models
{
    public class Oyunlar
    {
        public ObjectId Id { get; set; }

        [BsonElement("oyunNumarasi")]
        public int oyunNumarasi { get; set; }

        [BsonElement("oyunDurumu")]
        public int oyunDurumu { get; set; }

        [BsonElement("oyuncuSayisi")]
        public int oyuncuSayisi { get; set; }

        [BsonElement("oyunTarihi")]
        public string oyunTarihi { get; set; }

        [BsonElement("kazananOyuncuSayisi")]
        public int kazananOyuncuSayisi { get; set; }

        [BsonElement("sorular")]
        public Sorular[] sorular { get; set; }

        public Oyunlar()
        {
            sorular = new Sorular[10];
        }
    }
}