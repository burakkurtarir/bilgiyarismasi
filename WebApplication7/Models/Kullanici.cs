﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebApplication7.Models
{
    public class Kullanici
    {
        [BsonId]
        public ObjectId id { get; set; }

        [BsonElement("numara")]
        public int numara { get; set; }

        [BsonElement("kullaniciAdi")]
        public string kullaniciAdi { get; set; }

        [BsonElement("sifre")]
        public string sifre { get; set; }

        [BsonElement("yetki")]
        public int yetki { get; set; }

        [BsonElement("oyundaMi")]
        public bool oyundaMi { get; set; }

        [BsonElement("elendiMi")]
        public bool elendiMi { get; set; }

        [BsonElement("oyuncuDurumu")]
        public int oyuncuDurumu { get; set; }

        [BsonElement("puan")]
        public int puan { get; set; }
    }
}