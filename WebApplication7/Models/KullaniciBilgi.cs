﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication7.Models
{
    public class KullaniciBilgi
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("numara")]
        public int numara { get; set; }

        [BsonElement("sonKullaniciNumarasi")]
        public int sonKullaniciNumarasi { get; set; }
    }
}