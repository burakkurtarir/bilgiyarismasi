﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication7.Models
{
    public class SonKaldigimYer
    {
        public ObjectId Id { get; set; }

        [BsonElement("numara")]
        public int numara { get; set; }

        [BsonElement("sonKaldigimNumara")]
        public int sonKaldigimNumara { get; set; }
    }
}