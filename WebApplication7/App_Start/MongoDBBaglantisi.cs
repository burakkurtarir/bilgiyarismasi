﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;

namespace WebApplication7.App_Start
{
    public class MongoDBBaglantisi  //MongoDB bağlantısının yapıldığı class
    {
        public MongoClient mongoClient;
        public IMongoDatabase mongoDatabase;

        public MongoDBBaglantisi()
        {
            mongoClient = new MongoClient("mongodb://burak:merdiven123@ds039301.mlab.com:39301/bilgiyarismasidb");
            mongoDatabase = mongoClient.GetDatabase("bilgiyarismasidb");
        }
    }
}