﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication7.ModelViews
{
    public class OyunBilgileriModelView
    {
        public int oyunNumarasi { get; set; }

        public int oyunDurumu { get; set; }
        
        public int oyuncuSayisi { get; set; }

        public string oyunTarihi { get; set; }

        public int kazananOyuncuSayisi { get; set; }
    }
}