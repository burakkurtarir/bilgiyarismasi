﻿//Elementlerin tanımlanması
var soruNumarasiDiv, soruAdiDiv, aSikkiBtn, bSikkiBtn, cSikkiBtn, kalanZamanDiv, sonucDiv, yanlisYapanlarinSayisiDiv, dogryYapanlarinSayisiDiv;
soruNumarasiDiv = document.getElementById("soruNumarasi");
soruAdiDiv = document.getElementById("soruAdi");
aSikkiBtn = document.getElementById("aSikki");
bSikkiBtn = document.getElementById("bSikki");
cSikkiBtn = document.getElementById("cSikki");
kalanZamanDiv = document.getElementById("kalanZaman");
sonucDiv = document.getElementById("sonuc");
yanlisYapanlarinSayisiDiv = document.getElementById("yanlisYapanlarinSayisi");
dogruYapanlarinSayisiDiv = document.getElementById("dogruYapanlarinSayisi");

//Butonları deaktif yap
aSikkiBtn.disabled = true;
bSikkiBtn.disabled = true;
cSikkiBtn.disabled = true;

var cevabin = "D";  //Cevabın default değeri D şıkkı
var elendiMi = false;
var oyuncuDurumu;  //Veritabanından okunacak;
OyuncuDurumunuOku();  //Oyuncu durumunu serverdan okuyan fonksiyon

aSikkiBtn.addEventListener("click", function () {
    cevabin = "A";
    console.log("Cevabın: " + cevabin);
    //Butonları deaktif yap
    aSikkiBtn.disabled = true;
    bSikkiBtn.disabled = true;
    cSikkiBtn.disabled = true;
});
bSikkiBtn.addEventListener("click", function () {
    cevabin = "B";
    console.log("Cevabın: " + cevabin);
    //Butonları deaktif yap
    aSikkiBtn.disabled = true;
    bSikkiBtn.disabled = true;
    cSikkiBtn.disabled = true;
});
cSikkiBtn.addEventListener("click", function () {
    cevabin = "C";
    console.log("Cevabın: " + cevabin);
    //Butonları deaktif yap
    aSikkiBtn.disabled = true;
    bSikkiBtn.disabled = true;
    cSikkiBtn.disabled = true;
});


//Signalr bağlantısı
var myHub1 = $.connection.myHub1;

//Signalr admin mesajını alma fonksiyonu
myHub1.client.soruyuGonder = function (data) {
    //Admin soruyu gönderdiğinde
    SoruyuYaz(data);
    if (oyuncuDurumu == 1)  //Eğer yarışmacı ise
    {
        if (elendiMi == false) //Eğer elenmedi ise
        {
            SoruyuYaz(data); //Adminden gelen soru ekrana yazılıyor
            //Butonları aktif yap
            aSikkiBtn.disabled = false;
            bSikkiBtn.disabled = false;
            cSikkiBtn.disabled = false;
            var zaman = 10;  //10 saniyelik zaman başlatılıyor
            var oyunDongusu = setInterval(function () {
                console.log(zaman);
                kalanZamanDiv.innerText = "Zaman: " + zaman;
                zaman--;
                if (zaman <= 0) {
                    kalanZamanDiv.innerText = "Zaman doldu";
                    clearInterval(oyunDongusu);
                    $.post("/Admin/CevabiDondur", { "cevap": cevabin }, function (data) {
                        console.log("Doğru cevap: " + data);
                        if (data == cevabin) {
                            console.log("Cevabınız doğru");
                            sonucDiv.innerText = "Cevabınız doğru";
                        }
                        else {
                            console.log("Cevabınız yanlış, elendiniz");
                            sonucDiv.innerText = "Cevabınız yanlış, elendiniz";
                            elendiMi = true;
                        }
                    });
                }
            }, 1000);
        }

        else  //Eğer elendiyse
        {
            SoruyuYaz(data); //Adminden gelen soru ekrana yazılıyor
            var zaman = 10;  //10 saniyelik zaman başlatılıyor
            var oyunDongusu = setInterval(function () {
                console.log(zaman);
                kalanZamanDiv.innerText = "Zaman: " + zaman;
                zaman--;
                if (zaman <= 0) {
                    kalanZamanDiv.innerText = "Zaman doldu";
                    clearInterval(oyunDongusu);
                    $.post("/Admin/CevabiDondur", function (data) {
                        console.log("Doğru cevap: " + data);
                    });
                }
            }, 1000);
        }
    }
    else  //Eğer izleyici ise
    {
        SoruyuYaz(data); //Adminden gelen soru ekrana yazılıyor
        var zaman = 10;  //10 saniyelik zaman başlatılıyor
        var oyunDongusu = setInterval(function () {
            console.log(zaman);
            kalanZamanDiv.innerText = "Zaman: " + zaman;
            zaman--;
            if (zaman <= 0) {
                kalanZamanDiv.innerText = "Zaman doldu";
                clearInterval(oyunDongusu);
                $.post("/Admin/CevabiDondur", function (data) {
                    console.log("Doğru cevap: " + data);
                });
            }
            
        }, 1000);  
    }
  
}


var myHub2 = $.connection.myHub1;

myHub2.client.dogruYanlisSayisiniGoster = function (dogru, yanlis) {
    console.log("Sonuçlar geldi");
    console.log("Doğru yapanların sayısı: " + dogru);
    dogruYapanlarinSayisiDiv.innerText = "Doğru Yapan Kişi Sayısı: " + dogru;
    console.log("Yanlış yapanların sayısı: " + yanlis);
    yanlisYapanlarinSayisiDiv.innerText = "Yanlış Yapan Kişi Sayısı: " + yanlis;
}

var myhub3 = $.connection.myHub1;
myhub3.client.kazandiMi = function () {
    //Eğer hala elenmediyse kazanmış demektir, kullanıcı adı servera gönderilip kaydedilecek
    if (elendiMi == false) {  //Eğer elenmediyse
        var kullaniciAdi = localStorage.getItem("kullaniciAdi");
        $.post("/Admin/PuanVer", { "kullaniciAdi": kullaniciAdi }, function (data) {
            console.log("Yarışmayı kazandınız, yeni puanınız: " + data);
            sonucDiv.innerText = "Yarışmayı kazandınız, yeni puanınız: " + data;
        });
    }
    console.log(localStorage.getItem("kullaniciAdi"));
    console.log("Yarışma bitmiştir, katılımlarınız için teşekkürler");
    sonucDiv.innerText = "Yarışma bitmiştir, katılımlarınız için teşekkürler";
}

$.connection.hub.start();


function SoruyuYaz(data)
{
    soruNumarasiDiv.innerText = "Soru Numarası: " + data.soruNumarasi;
    soruAdiDiv.innerText = "Soru Adı: " + data.soruAdi;
    aSikkiBtn.innerText = "A) " + data.siklar[0];
    bSikkiBtn.innerText = "B) " + data.siklar[1];
    cSikkiBtn.innerText = "C) " + data.siklar[2];
}

function OyuncuDurumunuOku()
{
    $.post("OyuncuDurumunuOku", function (data) {
        oyuncuDurumu = data;
        console.log("Oyuncu durumu okundu");
    });
}


