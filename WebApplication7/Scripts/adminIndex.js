﻿//Elementlerin tanımlanması
var mesaj, mesajiGonderBtn;
var girisleriAcBtn, yarismayiBaslatBtn, yarismayiBitirBtn;
var soruNumarasiDiv, soruAdiDiv, aSikkiDiv, bSikkiDiv, cSikkiDiv, cevapDiv, soruyuGonderBtn, dogruYanlisSayiniGosterBtn;
mesaj = document.getElementById("mesaj");
mesajiGonderBtn = document.getElementById("mesajiGonder");
girisleriAcBtn = document.getElementById("girisleriAc");
yarismayiBaslatBtn = document.getElementById("yarismayiBaslat");
yarismayiBitirBtn = document.getElementById("yarismayiBitir");
soruNumarasiDiv = document.getElementsByClassName("soruNumarasi");
soruAdiDiv = document.getElementsByClassName("soruAdi");
aSikkiDiv = document.getElementsByClassName("aSikki");
bSikkiDiv = document.getElementsByClassName("bSikki");
cSikkiDiv = document.getElementsByClassName("cSikki");
cevapDiv = document.getElementsByClassName("cevap");
soruyuGonderBtn = document.getElementsByClassName("soruyuGonder");
dogruYanlisSayiniGosterBtn = document.getElementById("dogruYanlisSayiniGoster");

//Sayfa yüklendiği anda veritabanından sorular yüklenecek
SorulariYukle();

//Signalr bağlantısı
var myHub1 = $.connection.myHub1;


girisleriAcBtn.addEventListener("click", function () {
    //Oyun durumu açık yapılacak
    $.post("/Admin/OyunDurumunuDegistir", { "oyunDurumu": 1 }, function (data) {
        console.log("Girişler açıldı");
    });
    
});

yarismayiBaslatBtn.addEventListener("click", function () {
    //Oyun durumu oynanıyor yapılacak
    $.post("/Admin/OyunDurumunuDegistir", { "oyunDurumu": 2 }, function (data) {
        console.log("Yarışma başladı");
    });
   
});

yarismayiBitirBtn.addEventListener("click", function () {
    //Oyun durumu kapalı yapılacak
    $.post("/Admin/OyunDurumunuDegistir", { "oyunDurumu": 3 }, function (data) {
        console.log("Girişler kapatıldı");
    });
   
    //Kazanan yarışmacılar kaydedilecek
    $.connection.hub.start().done(function () {
        myHub1.server.kazandiMi();
    });
    //Her soru için doğru yanlış sayısı kayıt edilecek
    //Oyuna katılanların oyundaMi değeri false yapılacak
    $.post("/Admin/KullanicilariOyundanCikar", function (data) {
        console.log(data);
    });
    $.post("/Admin/YarismayiBitir", function (data) {
        console.log(data);
    });
});

//Kullanıcıya mesaj gönderme fonksiyonu
mesajiGonderBtn.addEventListener("click", function () {
    //Signalr kullanıcıya  mesaj gönderme fonksiyonu
    $.connection.hub.start().done(function () {
        myHub1.server.mesajGonder(mesaj.value);
    });
    console.log("Mesaj gönderildi");
});

dogruYanlisSayiniGosterBtn.addEventListener("click", function () {
    $.connection.hub.start().done(function () {
        myHub1.server.dogruYanlisSayisiniGoster();
    });
    console.log("Doğru yanlış sayısı gönderildi");
});

function SorulariYukle()
{
    //Sayfa yüklendiği anda sorular da yüklenecek
    $.post("/Admin/SorulariYukle", function (data) {
        console.log("Sorular yüklendi");
        for (let x = 0; x < 10; x++)
        {
            soruNumarasiDiv[x].innerText = "Soru Numarası: " + data[x].soruNumarasi;
            soruAdiDiv[x].innerText = "Soru Adı: " + data[x].soruAdi;
            aSikkiDiv[x].innerText = "A) " + data[x].siklar[0];
            bSikkiDiv[x].innerText = "B) " + data[x].siklar[1];
            cSikkiDiv[x].innerText = "C) " + data[x].siklar[2];
            cevapDiv[x].innerText = "Cevap: " + data[x].cevap;
            soruyuGonderBtn[x].addEventListener("click", function () {
                data[x].cevap = null;  //Kullanıcıya cevabı göndermemek için null'a eşitliyoruz
                $.connection.hub.start().done(function () {
                    myHub1.server.soruyuGonder(data[x]);
                });
                console.log(x + " numaralı soru gönderildi");
            });
        }
    });
}