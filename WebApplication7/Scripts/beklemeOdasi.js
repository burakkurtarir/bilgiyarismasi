﻿//Elementlerin tanımlanması
var oyunNumarasiP, oyunDurumuP, oyuncuSayisiP, adminMesajiP;
oyunNumarasiP = document.getElementById("oyunNumarasi");
oyunDurumuP = document.getElementById("oyunDurumu");
oyuncuSayisiP = document.getElementById("oyuncuSayisi");

adminMesajiP = document.getElementById("adminMesaji");

//Signalr bağlantısı
var myHub1 = $.connection.myHub1;

//Signalr admin mesajını alma fonksiyonu
myHub1.client.mesajGonder = function (data) {
    adminMesajiP.innerText = "Mesaj: " + data;
}

$.connection.hub.start();

//Sayfa yüklendiği anda, oyun bilgileri yüklenecek
OyunBilgileriniYukle();

//Oyun bilgilerini okuyan fonksiyon
function OyunBilgileriniYukle()
{
    $.post("/Anasayfa/OyunBilgileriniYukle", function (data) {
        console.log("Oyun bilgileri okundu");
        //Oyun bilgileri ekrana yazılıyor
        OyunBilgileriniYaz(data);
        console.log("Oyun bilgileri yazıldı");
    });
}

function OyunBilgileriniYaz(data)
{
    oyunNumarasiP.innerText = "Oyun Numarası: " + data.oyunNumarasi;
    if (data.oyunDurumu == 1) oyunDurumuP.innerText = "Oyun Durumu: Girişler Açık";
    else if (data.oyunDurumu == 2) oyunDurumuP.innerText = "Oyun Durumu: Oynanıyor";
    else if (data.oyunDurumu == 3) oyunDurumuP.innerText = "Oyun Durumu: Girişler Kapalı";
    //oyunDurumuP.innerText = "Oyun Durumu: " + data.oyunDurumu;
    oyuncuSayisiP.innerText = "Odadaki Oyuncu Sayısı: " + data.oyuncuSayisi;
}