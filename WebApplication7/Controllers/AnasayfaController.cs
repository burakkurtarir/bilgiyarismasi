﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication7.App_Start;
using WebApplication7.Models;
using MongoDB.Driver;
using System.Web.Security;
using WebApplication7.ModelViews;

namespace WebApplication7.Controllers
{
    public class AnasayfaController : Controller
    {
        public MongoDBBaglantisi mongoDbBaglantisi;
        public IMongoCollection<Kullanici> kullaniciCollection;
        public IMongoCollection<Oyunlar> oyunlarCollection;
        public IMongoCollection<SonKaldigimYer> sonKaldigimYerCollection;
        public IMongoCollection<KullaniciBilgi> kullaniciBilgiCollection;

        public AnasayfaController()
        {
            mongoDbBaglantisi = new MongoDBBaglantisi();
            kullaniciCollection = mongoDbBaglantisi.mongoDatabase.GetCollection<Kullanici>("Kullanici");
            oyunlarCollection = mongoDbBaglantisi.mongoDatabase.GetCollection<Oyunlar>("Oyunlar");
            sonKaldigimYerCollection= mongoDbBaglantisi.mongoDatabase.GetCollection<SonKaldigimYer>("SonKaldigimYer");
            kullaniciBilgiCollection = mongoDbBaglantisi.mongoDatabase.GetCollection<KullaniciBilgi>("KullaniciBilgi");
        }

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult BeklemeOdasi()
        {
            return View();
        }

        [Authorize]
        public ActionResult YarismaOdasi()
        {
            //Kullanıcı adı sessiondan okunuyor
            string kullaniciAdi = string.Empty;
            kullaniciAdi = Convert.ToString(Session["kullaniciAdi"]);
            //Kullanıcının oyunda olup olmadığına bakılıyor
            var kullaniciBilgileri = kullaniciCollection.Find(x => x.kullaniciAdi == kullaniciAdi).ToList();
            //Eğer kullanıcı adı sessiondan silinmişse veya veritabanında yoksa
            if (kullaniciBilgileri.Count == 0)
            {
                Session["yarismaOdasiBilgi"] = "Kullanıcı adınız bulunamadı";
                return RedirectToAction("BeklemeOdasi", "Anasayfa");
            }
            else if (kullaniciBilgileri[0].oyundaMi == false)  //Eğer kullanıcı oyunda değilse
            {
                //Önce son kaldığımız oyun numarasını bulucaz
                var sonKaldigimYer = sonKaldigimYerCollection.Find(x => x.numara == 1).ToList();
                //Sonra bu numaradaki oyun bilgilerini okuyoruz
                var oyunBilgileri = oyunlarCollection.Find(x => x.oyunNumarasi == sonKaldigimYer[0].sonKaldigimNumara).ToList();
                //Yarışma odasındaki oyuncu sayısı okunur
                int oyuncuSayisi = oyunBilgileri[0].oyuncuSayisi;
                if (oyunBilgileri[0].oyunDurumu == 1)  //Eğer oyun durumu açıksa
                {
                    //Yarışma odasına girenlerin oyundaMi değeri true yapılıyor
                    var filter = Builders<Kullanici>.Filter.Eq("kullaniciAdi", kullaniciAdi);
                    var update = Builders<Kullanici>.Update.Set("oyundaMi", true).Set("oyuncuDurumu", 1);
                    var result = kullaniciCollection.UpdateOne(filter, update);
                    //Yarışma odasındaki oyuncu sayısı 1 arttırılır
                    oyuncuSayisi++;
                    //Yarışma odasındaki oyuncu sayısı güncellenir
                    var filter1 = Builders<Oyunlar>.Filter.Eq("oyunNumarasi", sonKaldigimYer[0].sonKaldigimNumara);
                    var update1 = Builders<Oyunlar>.Update.Set("oyuncuSayisi", oyuncuSayisi);
                    var result1 = oyunlarCollection.UpdateOne(filter1, update1);
                    //Oyuncunun puanı session a atanıyor
                    Session["puan"] = kullaniciBilgileri[0].puan;
                    return View();
                }
                else  //Eğer oyun durumu kapalıysa
                {
                    Session["yarismaOdasiBilgi"] = "Oyun durumu kapalı veya oynanıyor";
                    return RedirectToAction("BeklemeOdasi", "Anasayfa");
                }
            }
            else  //Eğer kullanıcı oyundaysa
            {
                Session["yarismaOdasiBilgi"] = "Zaten oyundasınız";
                return RedirectToAction("BeklemeOdasi", "Anasayfa");
            }
        }

        //Giriş işleminin yapıldığı action
        [HttpPost]
        public ActionResult GirisYap(Kullanici k)
        {
            var kullanici = kullaniciCollection.Find(x => x.kullaniciAdi == k.kullaniciAdi &&
                                                    x.sifre == k.sifre).ToList();
            if (kullanici.Count == 1)  //Eğer giriş başarılıysa
            {
                FormsAuthentication.SetAuthCookie(k.kullaniciAdi, false);  //Giriş yapıldı
                Session["kullaniciAdi"] = k.kullaniciAdi;
                Session["girisBilgisi"] = "Giriş başarılı";
                if (kullanici[0].yetki == 1)  //Eğer admin ise
                {
                    return RedirectToAction("Index", "Admin");
                }
                else  //Eğer yarışmacı ise
                {
                    return RedirectToAction("BeklemeOdasi", "Anasayfa");
                }
            }
            else  //Eğer giriş başarısızsa
            {
                Session["kullaniciAdi"] = "Tanımsız";
                Session["girisBilgisi"] = "Kullanıcı adı veya şifre yanlış";
                return RedirectToAction("Index", "Anasayfa");
            }
        }

        public ActionResult KayitOl(Kullanici k)
        {
            var kullanici = kullaniciCollection.Find(x => x.kullaniciAdi == k.kullaniciAdi).ToList();
            if (kullanici.Count == 1)  //Eğer aynı kullanıcı adına sahip bir hesap varsa
            {
                Session["kayitBilgi"] = "Böyle bir kullanıcı zaten var";
                return RedirectToAction("Index", "Anasayfa");
            }
            else
            {
                //Son kullanıcının numarası alınıyor ki yeni kullanıcıya 1 fazlası verilsin
                var kullaniciBilgi = kullaniciBilgiCollection.Find(x => x.numara == 1).ToList();
                k.numara = kullaniciBilgi[0].sonKullaniciNumarasi;
                k.yetki = 2;
                k.oyundaMi = false;
                k.elendiMi = false;
                k.oyuncuDurumu = 3;
                k.puan = 0;
                //Kullanıcı veritabanına kaydedilir
                kullaniciCollection.InsertOne(k);
                Session["kullaniciAdi"] = k.kullaniciAdi;
                Session["kayitBilgi"] = "Başarıyla kayıt oldunuz";
                Session["puan"] = k.puan;
                FormsAuthentication.SetAuthCookie(k.kullaniciAdi, false);
                return RedirectToAction("BeklemeOdasi", "Anasayfa");
            }
        }

        public ActionResult CikisYap()
        {
            FormsAuthentication.SignOut();  //Çıkış yapıldı
            return RedirectToAction("Index", "Anasayfa");
        }

        [Route("/Anasayfa/OyunBilgileriniYukle")]
        public JsonResult OyunBilgileriniYukle()
        {
            //Önce son kaldığımız oyun numarasını bulucaz
            var sonKaldigimYer = sonKaldigimYerCollection.Find(x => x.numara == 1).ToList();
            //Sonra bu numaradaki oyun bilgilerini okuyoruz
            var oyunBilgileri = oyunlarCollection.Find(x => x.oyunNumarasi == sonKaldigimYer[0].sonKaldigimNumara).ToList();
            //Sadece oyun bilgilerini göndermek için, ModelView class'ı açtık ve bilgileri bu class'a aktardık(Amaç soruları göndermemek)
            OyunBilgileriModelView oyunBilgileriModelView = new OyunBilgileriModelView()
            {
                oyunNumarasi = oyunBilgileri[0].oyunNumarasi,
                oyunDurumu = oyunBilgileri[0].oyunDurumu,
                oyuncuSayisi = oyunBilgileri[0].oyuncuSayisi,
                oyunTarihi = oyunBilgileri[0].oyunTarihi,
                kazananOyuncuSayisi = oyunBilgileri[0].kazananOyuncuSayisi
            };
            return Json(oyunBilgileriModelView, JsonRequestBehavior.AllowGet);
        }

        [Route("/Anasayfa/OyuncuDurumunuOku")]
        public JsonResult OyuncuDurumunuOku()
        {
            //Kullanıcı adı sessiondan okunuyor
            string kullaniciAdi = string.Empty;
            kullaniciAdi = Convert.ToString(Session["kullaniciAdi"]);
            var kullaniciBilgileri = kullaniciCollection.Find(x => x.kullaniciAdi == kullaniciAdi).ToList();
            //Kullanıcı bilgileri okundu ve oyuncu durumu döndürüldü
            return Json(kullaniciBilgileri[0].oyuncuDurumu, JsonRequestBehavior.AllowGet);
        }

    }
}