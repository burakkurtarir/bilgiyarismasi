﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication7.App_Start;
using WebApplication7.Models;
using MongoDB.Driver;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace WebApplication7.Controllers
{
    public class AdminController : Controller
    {
        public MongoDBBaglantisi mongoDbBaglantisi;
        public IMongoCollection<Kullanici> kullaniciCollection;
        public IMongoCollection<Oyunlar> oyunlarCollection;
        public IMongoCollection<SonKaldigimYer> sonKaldigimYerCollection;
        public IMongoCollection<OyunlarBilgi> oyunlarBilgiCollection;

        public AdminController()
        {
            mongoDbBaglantisi = new MongoDBBaglantisi();
            kullaniciCollection = mongoDbBaglantisi.mongoDatabase.GetCollection<Kullanici>("Kullanici");
            oyunlarCollection = mongoDbBaglantisi.mongoDatabase.GetCollection<Oyunlar>("Oyunlar");
            sonKaldigimYerCollection = mongoDbBaglantisi.mongoDatabase.GetCollection<SonKaldigimYer>("SonKaldigimYer");
            oyunlarBilgiCollection = mongoDbBaglantisi.mongoDatabase.GetCollection<OyunlarBilgi>("OyunlarBilgi");
        }

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Route("/Admin/OyunDurumunuDegistir")]
        public JsonResult OyunDurumunuDegistir(int oyunDurumu)
        {
            //Önce son kaldığımız oyun numarasını bulucaz
            var sonKaldigimYer = sonKaldigimYerCollection.Find(x => x.numara == 1).ToList();
            //Sonra bu numaradaki oyun durumunu değiştircez
            var filter = Builders<Oyunlar>.Filter.Eq("oyunNumarasi", sonKaldigimYer[0].sonKaldigimNumara);
            var update = Builders<Oyunlar>.Update.Set("oyunDurumu", oyunDurumu);
            var result = oyunlarCollection.UpdateOne(filter, update);
            return Json("Oyun durumu değiştirildi", JsonRequestBehavior.AllowGet);
        }

        [Route("/Admin/SorulariYukle")]
        public JsonResult SorulariYukle()
        {
            //Önce son kaldığımız oyun numarasını bulucaz
            var sonKaldigimYer = sonKaldigimYerCollection.Find(x => x.numara == 1).ToList();
            //Sonra bu numaradaki oyun bilgilerini okuyoruz
            var oyunBilgileri = oyunlarCollection.Find(x => x.oyunNumarasi == sonKaldigimYer[0].sonKaldigimNumara).ToList();
            for (int i = 0; i < 10; i++)
            {
                YarismaTakibi.cevaplar[i] = oyunBilgileri[0].sorular[i].cevap;  //Cevapları static bir diziye atıyorum
                YarismaTakibi.dogruYapanlarinSayisi[i] = 0;  //İlk başta doğru ve yanlış yapanların sayısı 0'a eşitleniyor
                YarismaTakibi.yanlisYapanlarinSayisi[i] = 0;
            }
            //-1 oldu çünkü her gönderdiğimiz soru için 1 artacak ve 0'dan başlaması lazım
            YarismaTakibi.sonKaldigimizSoruNumarasi = -1;
            YarismaTakibi.kazananSayisi = 0;
            
            return Json(oyunBilgileri[0].sorular, JsonRequestBehavior.AllowGet);
        }

        [Route("/Admin/CevabiDondur")]
        public JsonResult CevabiDondur(string cevap)
        {
            //Son kaldığım soru numarasını static değerden okuyorum
            int sonKalidigimSoruNumarasi = YarismaTakibi.sonKaldigimizSoruNumarasi;
            if (cevap == YarismaTakibi.cevaplar[sonKalidigimSoruNumarasi])  //Eğer kullanıcın gönderdiği cevap doğruysa
            {
                YarismaTakibi.dogruYapanlarinSayisi[sonKalidigimSoruNumarasi]++;  //Doğru yapanları arttır
            }
            else  //Eğer kullanıcın gönderdiği cevap yanlışsa
            {
                YarismaTakibi.yanlisYapanlarinSayisi[sonKalidigimSoruNumarasi]++;  //Yanlış yapanları arttır
            }

            return Json(YarismaTakibi.cevaplar[sonKalidigimSoruNumarasi], JsonRequestBehavior.AllowGet);
        }

        [Route("/Admin/YarismayiBitir")]
        public JsonResult YarismayiBitir()
        {
            //Önce son kaldığımız oyun numarasını bulucaz
            var sonKaldigimYer = sonKaldigimYerCollection.Find(x => x.numara == 1).ToList();
            //Static class'taki doğru yanlış değerlerini veritabanına kaydediyoruz
            for (int i = 0; i < 10; i++)
            {
                var filter = Builders<Oyunlar>.Filter.Eq("oyunNumarasi", sonKaldigimYer[0].sonKaldigimNumara);
                var updateDogru = Builders<Oyunlar>.Update.Set(x => x.sorular[i].dogruYapanlarinSayisi, YarismaTakibi.dogruYapanlarinSayisi[i]);
                var updateYanlis = Builders<Oyunlar>.Update.Set(x => x.sorular[i].yanlisYapanlarinSayisi, YarismaTakibi.yanlisYapanlarinSayisi[i]);
                var resultDogru = oyunlarCollection.UpdateOne(filter, updateDogru);
                var resultYanlis = oyunlarCollection.UpdateOne(filter, updateYanlis);
            }
            
            //Kazanan sayısı kaydedilecek
            var filter1 = Builders<Oyunlar>.Filter.Eq("oyunNumarasi", sonKaldigimYer[0].sonKaldigimNumara);
            var update1 = Builders<Oyunlar>.Update.Set("kazananOyuncuSayisi", YarismaTakibi.kazananSayisi);
            var result1 = oyunlarCollection.UpdateMany(filter1, update1);

            //Son kaldığım oyun numarası 1 arttırılacak
            int sonKaldigimNumara = sonKaldigimYer[0].sonKaldigimNumara;
            sonKaldigimNumara++;
            var filter2 = Builders<SonKaldigimYer>.Filter.Eq("numara", 1);
            var update2 = Builders<SonKaldigimYer>.Update.Set("sonKaldigimNumara", sonKaldigimNumara);
            var result2 = sonKaldigimYerCollection.UpdateMany(filter2, update2);

            //Static class'ta tuttugumuz değerleri sıfırlıyoruz
            YarismaTakibi.sonKaldigimizSoruNumarasi = -1;
            for (int i = 0; i < 10; i++)
            {
                YarismaTakibi.dogruYapanlarinSayisi[i] = 0;
                YarismaTakibi.yanlisYapanlarinSayisi[i] = 0;
            }
            return Json("Yarışma bitti", JsonRequestBehavior.AllowGet);
        }

        [Route("/Admin/KullanicilariOyundanCikar")]
        public JsonResult KullanicilariOyundanCikar()
        {
            var filter = Builders<Kullanici>.Filter.Eq("oyundaMi", true);
            var update = Builders<Kullanici>.Update.Set("oyundaMi", false);
            var result = kullaniciCollection.UpdateMany(filter, update);
            return Json("Kullanıcılar oyundan çıkarıldı", JsonRequestBehavior.AllowGet);
        }

        public ActionResult SoruEkle()
        {
            return View();
        }

        [Route("/Admin/SoruyuKaydet")]
        public JsonResult SoruyuKaydet(string soru)
        {
            var myObj = new JavaScriptSerializer();
            Sorular[] sorular = myObj.Deserialize<Sorular[]>(soru);  //Kullanıcıdan soruları alıyoruz
            //Son kaldığım numarayı okuyup 1 arttırıyorum
            var oyunlarBilgi = oyunlarBilgiCollection.Find(x => x.numara == 1).ToList();
            int sonKaldigimNumara = oyunlarBilgi[0].sonKaldigimOyunNumarasi;
            sonKaldigimNumara++;
            Oyunlar oyunlar = new Oyunlar();
            oyunlar.oyunNumarasi = sonKaldigimNumara;
            oyunlar.oyunDurumu = 3;
            oyunlar.oyuncuSayisi = 0;
            oyunlar.kazananOyuncuSayisi = 0;
            oyunlar.oyunTarihi = DateTime.Now.ToString("dd.MM.yyyy");
            oyunlar.sorular = sorular;
            //Sorular veritabanına kaydediliyor
            //oyunlarCollection.InsertOne(oyunlar); 
            //Son kaldığım numarayı güncelliyorum
            var filter = Builders<OyunlarBilgi>.Filter.Eq("numara", 1);
            var update = Builders<OyunlarBilgi>.Update.Set("sonKaldigimOyunNumarasi", sonKaldigimNumara);
            var result = oyunlarBilgiCollection.UpdateMany(filter, update);
            return Json(oyunlar, JsonRequestBehavior.AllowGet);
        }

        [Route("/Admin/PuanVer")]
        public JsonResult PuanVer(string kullaniciAdi)
        {
            //Kullanıcıyı buluyoruz ve puanını 10 arttırıyoruz
            var kullaniciBilgileri = kullaniciCollection.Find(x => x.kullaniciAdi == kullaniciAdi).ToList();
            int puan = kullaniciBilgileri[0].puan;
            puan += 10;
            var filter = Builders<Kullanici>.Filter.Eq("kullaniciAdi", kullaniciAdi);
            var update = Builders<Kullanici>.Update.Set("puan", puan);
            var result = kullaniciCollection.UpdateMany(filter, update);
            //Kazanan sayısını 1 arttırıyoruz
            YarismaTakibi.kazananSayisi++;
            return Json(puan, JsonRequestBehavior.AllowGet);
        }
    }
}